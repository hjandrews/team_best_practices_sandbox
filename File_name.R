#This script calculates all variables used in the insect abundance analysis. 
#It is based on the data/variables&transformations from "CDD_Calculation.r"
library(readr)
library(tidyverse)
library(lubridate)
library(ggplot2)
options(scipen = 999)

#SET GGPLOT DEFAULT THEMES & COLOR PALETTE
source("ggplot_format_source.r")
#Set up labels for each site to appear on plot
# library(directlabels)
# last.qp <- vertical.qp("last.points")
# first.qp <- vertical.qp("first.points")

###SITE CHARACTERISTIC DATA
#sites <- read_csv("Raw_data/Locations.csv", na = "NA")
sites <- read_csv("Raw_data/Locations_elev_sort.csv", na = "NA")
#alt_sitecode <- read_csv("T:/Groups/sjohnsonlab/Stephanie/Insect_Phenology/CSVs/alt_sitecode.csv")
#Ordered by elevation
sites$sitecode = factor(sites$sitecode, levels = c("PC1","PC4","PC2","PC5","PC8","PC7","PC14","PC15", "PC9",  "PC10", "PC16", "PC12", "PC11", "PC13", "PC17", "PC18"))
#####GDD
##Read in hourly air temperature file - MS045 entity 2 and set data types
#Excel altered from download from MS045: removed DBCODE, ENTITY, SITETYPE, QC_LEVEL;
#changed column names, removed leading zero from sitecodes
#hourly <- read_csv("Raw_data/MS04502_Hourly_edit.csv", na = "NA")
#hourly = filter(hourly, sitecode != "")
#Order by elevation
# hourly$sitecode = factor(hourly$sitecode, levels = c("PC1","PC4","PC2","PC5","PC8","PC7","PC14","PC15", "PC9",  "PC10", "PC16", "PC12", "PC11", "PC13", "PC17", "PC18"))
# hourly$date = as.POSIXct(hourly$date, format = "%m/%d/%Y %H:%M", tz = "America/Los_Angeles")
# hourly = filter(hourly, is.na(hourly$date) != TRUE)
# hourly$date <- as.Date(hourly$date, tz = "America/Los_Angeles")
#Flag values: A = accepted; E = estimated; M = missing; Q = questionable
#hourly$airtemp_flag = factor(hourly$airtemp_flag)

##Save to RDS file
#saveRDS(hourly, file ="RDS_files/hourly_raw_manuscript.rds")

#Read in RDS file & change column names
hourly = readRDS("RDS_files/manuscript/hourly_raw_manuscript.rds")

#Filter to include only months and years relevant to study/GDD calc (794016 rows)
hourly <- filter(hourly, date >= "2008-12-01" & date < "2014-7-31")

#Compute daily mean (average) hourly temperature
daily = hourly %>%
  group_by(sitecode, date) %>%
  summarise(daily_avg = mean(airtemp))

#Review flags
#No missing or questionable values during the study period
#View dates with estimated temps by site
#Temps estimated until ~7/2009 at all sites; 
#PC1, PC5, & PC11 temps estimated for extended periods in 2013/2014
# filter(hourly, airtemp_flag == "E") %>%
#   ggplot(aes(date, airtemp_flag)) +
#    geom_point() +
#    scale_x_date(breaks = "6 months") +
#    pf +
#    theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
#    facet_wrap(.~sitecode)
# 
# est_temp = hourly %>%
#   filter(airtemp_flag == "E") %>%
#   group_by(sitecode) %>%
#   summarise(est_obs = n())
# 
# est_temp$pct_est = (est_temp$est_obs/794016)*100

#Daily growing degree-days (GDD) were calculated by summing all hourly mean temps >5°C (threshold) 
#in a 24-h period and dividing that sum by the number of hourly temp observations for that day (usually 24)) 
#dh = if hourly mean temp > 5, subtract 5 (threshold) to get degree hours, otherwise 0
hourly$dh = ifelse(hourly$airtemp > 5, hourly$airtemp-5, 0)

#Compute daily GDD using dh for each site
#dh_sum = total hourly degrees > 5C, n = # of temp records for the day, dd = average daily GDD
dd = hourly %>%
  group_by(sitecode, date) %>%
  summarise(dh_sum = sum(dh, na.rm = TRUE), n = n(), dd = mean(dh, na.rm = TRUE))

#Subset by sampling year to calculate GDD accumulation
year1 = subset(dd, date >= as.Date("2008-12-01") & date < as.Date("2009-07-31"))
year2 = subset(dd, date >= as.Date("2009-12-01") & date < as.Date("2010-07-31"))
year3 = subset(dd, date >= as.Date("2010-12-01") & date < as.Date("2011-07-31"))
year4 = subset(dd, date >= as.Date("2011-12-01") & date < as.Date("2012-07-31"))
year5 = subset(dd, date >= as.Date("2012-12-01") & date < as.Date("2013-07-31"))
year6 = subset(dd, date >= as.Date("2013-12-01") & date < as.Date("2014-07-31"))

#Calculate the cumulative sum of GDD for each sampling year (gdd), 
#create column with sampling year
year1 = year1 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2009")
year2 = year2 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2010")
year3 = year3 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2011")
year4 = year4 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2012")
year5 = year5 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2013")
year6 = year6 %>%
  group_by(sitecode) %>%
  mutate(gdd = cumsum(dd), year = "2014")

#Join data for all sampling years
gdd = full_join(year1,year2)
gdd = full_join(gdd,year3)
gdd = full_join(gdd,year4)
gdd = full_join(gdd,year5)
gdd = full_join(gdd,year6)
rm(year1,year2,year3,year4,year5,year6)

#Create column with month as a factor for plotting
#(necessary as lubridate::month sets months ordinally with Dec after July)
gdd = gdd[,c(1:2, 6:7)]
gdd = mutate(gdd, month = as.character(lubridate::month(date, label = TRUE, abbr = TRUE)))
gdd$month = factor(gdd$month, levels = c("Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul"))
gdd$doy = lubridate::yday(gdd$date)

#Compute cumulative GDD on doy 181 (~July 1) for each site and sampling year
sumdd = gdd %>%
  group_by(sitecode, year) %>%
  filter(doy == 181)

sumdd$year = factor(sumdd$year)

#Join with total cumulative GDD data
dd_doy_181 = left_join(sites, sumdd, by = "sitecode")

###PRECIP DATA
#Edited from raw data - unnecessary columns, renamed columns
#precip <- read_csv("T:/Groups/sjohnsonlab/Stephanie/Insect_Phenology/Raw_data/primet_precip_edit.csv", 
#                   na = "NA")
#saveRDS(precip, "RDS_files/manuscript/precip.rds")
precip = readRDS("RDS_files/manuscript/precip.rds")
#Format datetime
precip$date = as.Date(precip$date, format = "%m/%d/%Y", tz = "America/Los_Angeles")
#Filter out unnecessary data (outside of 2009 start of sampling/end of 2014 sampling)
precip <- filter(precip, date >= "2009-5-01" & date < "2014-07-31")
#Add year/month column as factors
precip$year = factor(year(precip$date))
precip$month = factor(month(precip$date, label = TRUE))
  
# #Total monthly precip faceted by year
# precipmon = precip %>%
#   group_by(year, month) %>%
#   summarise(PRECIP_TOT_month = sum(PRECIP_TOT_DAY))

#Total precip during April-June
# sampling_precip = precipmon %>%
#   filter(month == "Apr" | month == "May" | month == "Jun") %>%
#   group_by(year) %>%
#   summarise(sampling_precip = sum(PRECIP_TOT_month))

###INSECT DATA
#Read in insect raw data - source /Raw_data/SA02501_v2_insect_raw.csv
# library(readr)
# insect <- read_csv("Raw_data/SA025_insect_raw_edit.csv", na = "NA")
# saveRDS(insect, "RDS_files/manuscript/insects_raw.rds")
insect = readRDS("RDS_files/manuscript/insects_raw.rds")
#Remove comments column
insect = insect[,-12]

#Format dates
insect$set_date = as.Date(insect$set_date, format = "%m/%d/%Y", tz = "America/Los_Angeles")
insect$pickup_date = as.Date(insect$pickup_date, format = "%m/%d/%Y", tz = "America/Los_Angeles")

#Make sitecode factor with levels ordered by elevation
insect$sitecode = factor(insect$sitecode, levels = c("PC1","PC4","PC2","PC5","PC8","PC7","PC14","PC15", "PC9",  "PC10", "PC16", "PC12", "PC11", "PC13", "PC17", "PC18"))

#Make sitecode, year factor, add month factor
insect$year = factor(as.character(insect$year))
insect$month = factor(lubridate::month(insect$pickup_date, label = TRUE, abbr = TRUE))

#Use if mapping sites to nearest met station
#insect$sitecode = ifelse(insect$SITE_ID %in% c("PC001","PC002","PC004","PC005","PC007"), 
#                    "CS2MET", ifelse(insect$SITE_ID %in% c("PC008","PC009","PC010","PC011","PC012","PC013"), "CENMET", "H15MET"))

#Add day of year column based on pickup date
insect$doy = lubridate::yday(insect$pickup_date)

#Look at sampling balance among sites
# sampbal = insect %>%
#   group_by(year, month, sitecode) %>%
#   summarise(days_set = sum(days_set), n = n())

#2009 pretty unbalanced - between 2-13 samples (13-51 total sampling days)
# sampbal_2009 = sampbal %>%
#   filter(year == 2009) %>%
#   group_by(sitecode) %>%
#   summarise(days_set = sum(days_set), n = sum(n))

#Remove dates to even sampling
# sampbal_even = insect %>%
#   filter(doy > 120 & doy < 180) %>%
#   group_by(year, month, sitecode) %>%
#   summarise(days_set = sum(days_set), n = n())

# sampbal_even = insect %>%
#   mutate(month_num = as.integer(month)) %>%
#   filter(month_num > 1 & month_num < 5) %>%
#   group_by(year, month, sitecode) %>%
#   summarise(days_set = sum(days_set), n = n())

# #Summary with total winged count by year/month/site, not normalized by trapping days
# sumabd = insect %>%
#   group_by(sitecode, year, month) %>%
#   #filter(month > "Mar" & month < "Jul") %>%
#   summarise(total_abd = sum(total_abd), scellus = sum(scellus),
#             bolbo = sum(bolbo), anaspis = sum(anaspis), n = n())
# 
# #Summary with normalized total winged count by year/month/site, normalized by trapping days
# sumabdnorm = insect %>%
#   group_by(year, month, sitecode) %>%
#   summarise(days_set = sum(days_set), total_abd = sum(total_abd)/sum(days_set), scellus = sum(scellus)/sum(days_set),
#             bolbo = sum(bolbo)/sum(days_set), anaspis = sum(anaspis)/sum(days_set), n = n())
# 
# #Get normalized abundance stats for each year/mon/site
# abdnormmon = sumabdnorm %>%
#   group_by(year, month) %>%
#   summarise(diff = max(total_abd-min(total_abd)))

####LINK INSECT AND CLIMATE DATA
#If no changes made to lines above, can just read in this file to avoid waiting for loops to run
#insect = readRDS("insect_after_loops.rds")

#Calculate total precip from set_date to day before pickup_date
insect$tot_precip = NA
for (i in 1:length(insect$pickup_date)) {
  days = seq(insect$set_date[i], insect$pickup_date[i], by = "days")
  rainday = pmatch(days[], precip$date, dup=FALSE)
  insect$tot_precip[i] = sum(precip$precip_mm[(min(rainday)):(max(rainday)-1)])
}
rm(i,days,rainday)
insect_forloop1 = insect

#Calculate proportion of sampling days with measurable precip
insect$prop_precip = NA
precip$meas_precip = ifelse(precip$precip_mm > 0, 1, 0)
for (i in 1:length(insect$pickup_date)) { 
  days = seq(insect$set_date[i], insect$pickup_date[i]-1, by = "days") #List days including set date - day before pickup date
  rainday = filter(precip, date %in% days)
  rainday = sum(rainday$meas_precip)
  insect$prop_precip[i] = rainday/(insect$days_set[i]) 
}
rm(i,days,rainday)
insect_forloop2 = insect

#Calculate average daily average temp from set_date to day before pickup_date
# insect$daily_avg = NA
# for (i in 1:length(insect$pickup_date)) {
#   days = seq(insect$set_date[i], insect$pickup_date[i], by = "days")
#   tempday = pmatch(days[], daily$date, dup=FALSE)
#   insect$daily_avg[i] = sum(daily$daily_avg[(min(tempday)):(max(tempday)-1)])
# }
# rm(i,days,tempday)
# insect$daily_avg = insect$daily_avg/insect$days_set
# insect_forloop3 = insect

#Calculate hourly average temp from set_date to day before pickup_date
insect$hourly_avg = NA
for (i in 1:length(insect$pickup_date)) {
  days = seq(insect$set_date[i], insect$pickup_date[i]-1, by = "days")
  tempday = filter(hourly, sitecode == insect$sitecode[i] & date %in% days)
  insect$hourly_avg[i] = mean(tempday$airtemp)
}
rm(i,days,tempday,hourly)
insect_forloop4 = insect

#Write this file if changes made above
saveRDS(insect_forloop4, "RDS_files/manuscript/insect_after_loops.rds")
remove(insect_forloop1,insect_forloop2,insect_forloop3,insect_forloop4)

#Add climate data relevant to sampling period & remove month,year,doy from climate data
climate.join = filter(gdd, date >= as.Date(min(insect$set_date)) & date <= as.Date(max(insect$pickup_date)))
#Remove year, month, day columns
climate.join = climate.join[,-c(4:6)]

#Create new df with insect & climate data
joined = insect

#Rename "pickup_date" to "date" so climate & insect data can be joined by that column
colnames(joined)[6] = "date"

#Join all data
joined = left_join(joined, climate.join, by = c("sitecode","date"))
rm(climate.join)

#Change "date" back to "pickup_date"
colnames(joined)[6] = "pickup_date"

#Create column with normalized insect abundance 
joined = mutate(joined, norm_abd = total_abd/days_set)

#Add cumulative total insect abundance & precip columns
joined = joined %>%
  group_by(year, sitecode) %>%
  mutate(cum_tot_abd = cumsum(total_abd))

#Add site characteristics to data (leave out lat/long)
joined = left_join(joined, sites[,c(1,4:8)], by = "sitecode")

#Based on elevation vs. GDD and daily avg temp, cluster groups into low-med-high groups
#Larger difference between low-medium than med-high, but maybe still useful?
joined = joined %>% 
  mutate(el_group = ifelse(elev < 650, "Low", ifelse(elev < 1200, "Medium", "High")))
joined$el_group = factor(joined$el_group, levels = c("Low","Medium","High"))

#Create column with elevation as factor
joined$elev_fac = factor(joined$elev, levels = unique(joined$elev))

#Look at gdd by elev / stage on day 185
# gdd = left_join(gdd,sites,by="sitecode")
# gdd$year = year(gdd$date)
# gddelev = gdd %>%
#   group_by(STAGE,ELEV) %>%
#   filter(doy == 185) %>%
#   summarise(min = min(gdd),max = max(gdd)) %>%
#   mutate(range = max-min)
# 
# gddelev$elev_fac = factor(gddelev$ELEV, levels = unique(gddelev$ELEV))

#Calculate cumulative total trapping days (cum_days_set) and ratio of cum_tot_abd:cum_days_set
joined = joined %>%
  group_by(year, sitecode) %>%
  mutate(cum_days_set = cumsum(days_set))
joined$cum_norm_abd = round((joined$cum_tot_abd/joined$cum_days_set), digits = 0)

#Calculate the accumulated normalized abundance
joined = joined %>%
  group_by(year, sitecode) %>%
  mutate(norm_abd_accum = cumsum(norm_abd))

#Reorder columns in main df
col_order <- c("sample_id", "trap_no", "sitecode","year","month","doy",
               "set_date","pickup_date","days_set","cum_days_set","total_abd","norm_abd","cum_tot_abd","cum_norm_abd",
               "norm_abd_accum","scellus","bolbo","anaspis","gdd","hourly_avg","tot_precip","prop_precip",
               "elev","slope","aspect","stage","TPI","el_group","elev_fac")
joined = joined[, col_order]
rm(col_order)

#Add data on last day of year with snow cover
# significant = cover >50% and deep enough to bury herbs
#snow <- read_csv("CSVs/snow.csv", na = "NA")
#Mark corrected sent 12/29/2019
snow <- read_csv("CSVs/snow_corrected.csv", na = "NA")
#snow = filter(snow, year != "2009")
snow$year = factor(snow$year)
snow$sitecode = factor(snow$sitecode, levels = c("PC1","PC4","PC2","PC5","PC8","PC7","PC14","PC15", "PC9",  "PC10", "PC16", "PC12", "PC11", "PC13", "PC17", "PC18"))
#snow$sitecode = factor(snow$sitecode, levels = c("PC1","PC2","PC4","PC5","PC7","PC8","PC9","PC10","PC11","PC12","PC13","PC14","PC15","PC16","PC17","PC18"))
joined = left_join(joined, snow, by = c("sitecode", "year"))

#Adding a line to simulate a code change for the case example
#Changing to simulate post-comment changes for case example
joined = joined %>%
  group_by(year, sitecode) %>%
  mutate(cum_days_set = cumsum(days_set))
joined$cum_norm_abd = round((joined$cum_tot_abd/joined$cum_days_set), digits = 0)

#Calculate the accumulated normalized abundance
joined = joined %>%
  group_by(year, sitecode) %>%
  mutate(norm_abd_accum = cumsum(norm_abd))

#Reorder columns in main df
col_order <- c("sample_id", "trap_no", "sitecode","year","month","doy",
               "set_date","pickup_date","days_set","cum_days_set","total_abd","norm_abd","cum_tot_abd","cum_norm_abd",
               "norm_abd_accum","scellus","bolbo","anaspis","gdd","hourly_avg","tot_precip","prop_precip",
               "elev","slope","aspect","stage","TPI","el_group","elev_fac")
joined = joined[, col_order]
rm(col_order)
#Reorder columns in main df
col_order <- c("sample_id", "trap_no", "sitecode","year","month","doy",
               "set_date","pickup_date","days_set","cum_days_set","total_abd","norm_abd","cum_tot_abd","cum_norm_abd",
               "norm_abd_accum","scellus","bolbo","anaspis","gdd","hourly_avg","tot_precip","prop_precip",
               "elev","slope","aspect","stage","TPI","el_group","elev_fac")
               
#NOT WORKING
#GDD and snow factor levels for year not aligning so getting na on join
# snow <- read_csv("CSVs/snow.csv", na = "NA")
# snow = filter(snow, year != "2009")
# snow$year = factor(snow$year)
# snow$sitecode = factor(snow$sitecode, levels = c("PC1","PC2","PC4","PC5","PC7","PC8","PC9","PC10","PC11","PC12","PC13","PC14","PC15","PC16","PC17","PC18"))
# gdd$sitecode = factor(gdd$sitecode, levels = c("PC1","PC2","PC4","PC5","PC7","PC8","PC9","PC10","PC11","PC12","PC13","PC14","PC15","PC16","PC17","PC18"))
# gdd$year = factor(gdd$year)
# gdd2 = left_join(gdd, snow, by = c("sitecode", "year"))


# gdd_b4_snowmelt = gdd %>%
#   group_by(year, sitecode) %>%
#   filter(doy == sig_cover_doy)
# colnames(gdd_b4_snowmelt)[3] = "gdd_snowmelt"
# gdd_b4_snowmelt = gdd_b4_snowmelt[,c(1,3,4)]
# joined = left_join(joined, gdd_b4_snowmelt, by = c("sitecode","year"))

#Make new sitecode name with elevation
#joined = left_join(joined, site_and_alt_site_id, by = "sitecode")


#NOT WORKING YET
#Add winter & spring GDD from Ward paper data 
# winter_spring_gdd <- read_csv("Raw_data/winter_spring_gdd_from_Ward.csv", 
#                               col_types = cols(year = col_character()), na = "NA")
# winter_spring_gdd = filter(winter_spring_gdd, year!='2015')
# winter_spring_gdd = filter(winter_spring_gdd, year!='2016')
# winter_spring_gdd$sitecode = factor(winter_spring_gdd$sitecode)
# winter_spring_gdd$year = factor(winter_spring_gdd$year)
# joined2 = left_join(joined, winter_spring_gdd, by = c("sitecode","year"))
# joined2 = joined2 %>%
#   group_by(year, sitecode) %>%
#   mutate(diff = max(gdd) - (winter_gdd + spring_gdd))

#If anything changed above, resave files
# saveRDS(joined, ".//RDS_files//manuscript//insect_all_data.rds")
# saveRDS(gdd, ".//RDS_files//manuscript//gdd.rds")
# saveRDS(sites, ".//RDS_files//manuscript//sites.rds")

#Just focal taxa melted
#????Remove total_abd-related columns
#focal = joined[,-c(10:13,30)]
focal = joined
focal = focal %>%
  group_by(year, sitecode) %>%
  mutate(scellus_cum_norm = cumsum(scellus/days_set)) %>%
  mutate(bolbo_cum_norm = cumsum(bolbo/days_set)) %>%
  mutate(anaspis_cum_norm = cumsum(anaspis/days_set))
focal.melt = gather(focal, key = "Genera", value = "Abundance", 16:18)
# saveRDS(focal, ".//RDS_files//manuscript//focal.rds")
# saveRDS(focal.melt, ".//RDS_files//manuscript//focal_melt.rds")

#Create dataset for 2009 with taxa ID info
family_2009 = filter(joined, year == "2009")
family_2009_count <- read_csv("T:/Groups/sjohnsonlab/Stephanie/Insect_Phenology/CSVs/family_2009_count.csv", na = "NA")
family = left_join(joined, family_2009_count, by = "sample_id")
family = pivot_wider(family, names_from = "family", values_from = "fam_total_abd")
remove(family_2009, family_2009_count)
#saveRDS(family, ".//RDS_files//manuscript//family_2009.rds")

#Calculate dd accumulated in sampling period
# dd = filter(dd, date >= "2009-05-05" & date <= "2014-07-02")
# joined$dd_tot = NA
# 
# for (i in 1:length(insect$pickup_date)) {
#   days = seq(insect$set_date[i], insect$pickup_date[i], by = "days") #Creates vectors with all dates in trapping period
#   tempday = pmatch(days[], dd$date, dup=FALSE) #Creates vector of dates that match between insect & daily temp dfs
#   joined$dd_tot[i] = sum(dd$dd[(min(tempday)):(max(tempday)-1)]) #Creates column with daily avg for trapping period
# }
# 
# rm(i,days,tempday)
# joined.restrict = left_join(joined.restrict, joined)
