# Team Best Practices Sandbox
This is a git repository for groups working with the H.J. Andrews 
LTER to practice using git for collaborative science research and 
to document best practices for the research community. 

[See our ReadTheDocs!](https://team-best-practices-sandbox.readthedocs.io/)

The best way to learn is by doing. So here we aim to provide 
a place to practice as well as demonstrate. 
We placed some of our best code in this repository, some great 
code from professional programmers, and some 
code that was just lying around. None of them are production 
copies, they're just examples of what to do and what not to do. 
So it is o.k. if users edit them poorly or break features. 
That just gives the community an opportunity to practice using 
git and documentation to restore what was broken or deleted. 

As we figure out how to do things better, we want to build on the 
documentation so that others can learn from our mistakes (and so 
that we don't forget them). See examples of what we've learned so 
far in ReadTheDocs which compiles the ./doc file of this 
repository into a more readable format. 

### Note
The readme file uses bitbucket flavored Markdown (md). 
Typing the same syntax will be rendered differently by Bitbucket 
than it would be by Github, Drupal/PHP (Markdown Extra), Sourceforge, 
or Reddit. They all are a little different. For that reason, the 
./docs are written in ReStructuredText (rst), which isn't quite as 
simplistic, but doesn't have all the flavor/dialects to navigate. 
Plus it gives a wider variety of examples to learn from!
