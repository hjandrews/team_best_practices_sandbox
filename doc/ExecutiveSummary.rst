*************************************************
Example Executive Summary- Climate Program Precip
*************************************************
How to use this example
#######################
This is an example of a 2 page summary of a much more complicated
analysis. Providing an overview is always more helpful than you
think.

This short document uses standard ReStructuredText (rst) format.
It includes syntax for headings (chapter, section, sub-section,
etc.) numberd lists, bullet points, hyperlinks, and probably
a few others.

This section shows up first in the final documentation because
it is listed first in index.rst.

Summary
#######

Sheltertop and Stand Alone rain gauges at HJA benchmark climate 
stations (UPLO, CENT, VARA) use industrial tank gauges 
(analog level transmitters) 
to record precipitation. Both styles of rain gauge have a heated 
orifice that feeds into a smaller, heated, collection tank, 
amplifying the 
increase in the tank for a given amount of precipitation.
The tank gauge measures the current tank depth, which is then 
processed through QA programs to identify accumulation of 
precipitation.

2 of the industrial tank gauges began to fail in WY2019. Any 
repalcement must fit in our installation and maintain the 
precision and accuracy of our dataset.

Sizes of orifice, tank, and instrument
#######################################

The first step is to understand the current tank size and 
its relationship to orifice size.

The closest recreation of the coefficients currently applied 
at the stations relies on several assumptions, that contradict 
most of the formal documentation derived from FSDB and other 
sources posted on our website: 

#. SH orifice is 13.3"
     a. measured at UPLO
     b. consistent throughout documentation
#. SA orifice is 19.75" at UPLO/CENT
    a. Implied in EDLOG program notes for VARA
    b. Contradicts *all* `current docs`_ , as well as older
       `method documentation`_
#. SH tank gague length is 53"
    a. Required for calculating coefficient
    b. Contradicts UPLO Establishment Report
    c. Taken from on-site instrument label
#. SA tank gague length is 113"
    a. Taken from on-site instrument label at UPLO/VARA
    b. contradicts `method documentation`_
#. Assumed that the mounting distance was set at 5" and is 
   subtracted from instruemnt range (in addition to 3" dead 
   zone).
   
Tank gauge accuracy
###################

Current instrument specifications, adjusted for conversion 
between orifice and well. Numbers represent potential errors 
in mm of precipitation:
 
 * Stand alone: accurate +-0.2794 mm and repeatable within +- 0.0803 mm
 * Shelter: accurate +-0.3810 mm and repeatable within +- 0.1120 mm

The worst case shelter measurement would be +- 0.4930 mm

Total measurment error observed within our record from 2015- 2020 
was assessed 
by looking at average change during known dry periods when the 
true change should be 0. I 
suggest the mean can be interpreted as accuracy, the standard 
deviation as precision, and the that the two combined 
represent total measurement error. The observation perdiod had 
lower meaurment error than sensor specs.

* Stand alone: mean -0.0011 mm, StdDev 0.0861, total error 0.0872 mm
* Shelter: mean -0.0029, StdDev 0.1922, total error 0.1951 mm

A spot check of UPLO SA from 1996 - 2005 showed that the sensor 
has at least been within it's manufacturing specs since 
instalation, though there was 4 - 5 x more error in this earlier 
period. 

It was also found that the measurement **error could be 5x 
smaller if we averaged our 
measurements**; either repeating the sample multiple times, or 
taking a mean of 15 second samples at the 5 min record.

Replacement sensor
###################

A **tipping bucket is the most accurate and precise replacement 
sensor as 
well as the most affordable**. In the shelters, it is trivial 
to position the tipping bucket beneath the hose coming from 
the orifice, and so that the tip empties into the tank. This 
would continue to allow a redundant monthly total to be 
calculated from the tank sight-gauge.

However there is no practical installation in the stand alone. 
For the stand alone, the only replacement that doesn't reduce 
precision and accuracy is the MTS LPR, an upgrade of our current 
MTS tank gauge.

.. _`current docs`: http://andlter.forestry.oregonstate.edu/data/enume.aspx?domain=enum&dbcode=MS001&enumid=15487
.. _`method documentation`: https://andrewsforest.oregonstate.edu/sites/default/files/lter/data/studies/ms01/meta/instlist.htm#PPT_SAR1
