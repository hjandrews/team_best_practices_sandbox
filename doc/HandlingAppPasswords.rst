***********************
Handling App Passwords
***********************

Bitbucket will no longer allow git bash or git GUI to access your cloud based repositories using your username and login
. Instead, you will have to create a onetime App password to assign to a program. They have information about how to do
it, but I had a lot of problems. I had to untangle a lot of layered local, global, and system configs for the git
program, so here is a 15 min summary of me banging my head on the desk and winning.

`Atlassian: Create an app password <https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/#Create-an-app-password>`_

`Atlassian support thread <https://community.atlassian.com/t5/Bitbucket-questions/How-to-set-app-password-in-Git-Bash-command-prompt/qaq-p/1936375>`_

#. Set remote url to use bitbucket username, not email. For example, md22 not matt.dillon\@osu.edu. You may need to change this in both bash and SourceTree
    #. In SourceTree see Settings>>Remotes and select a remote account
    #. In Bash

        .. code-block::

            git remote get-url origin
            git remote set-url origin https://<username>@bitbucket.org/hjandrews/<repo>.git
            git remote get-url origin

#. Set email to <username>@bitbucket.org. In Source tree it can be changed in repository settings.

    .. code-block::

        git config user.email
        git config --global user.email "<username>@bitbucket.org"
        git config user.email

#. Make sure you don't have old passwords stored anywhere.
    #. First check and remove from git config.

    .. code-block::

        git config --list
        git config user.password
        git config --unset user.password

    #. Then go to any .git-credentials files and delete any passwords or \*.lock files. I spent almost and hour searching for one hidden on N:/home

        .. code-block::

            git config --list --show-origin

#. Create a new app password in your Bitbucket account
#. Setting the new App Password:
    #. First, in SourceTree, go to Tools>>Options>>Authentication and set Basic or OAuth (worked for me)
    #. Then go to ControlPanel:CredentialManager>>WindowsCredentials (you can hit the windows key and just type CredentialManager)
    #. Then add or edit `git:https://bitbucket.org`. Insert the AppPassword here