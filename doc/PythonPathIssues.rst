************************
Issues with Python Paths
************************

Newer programs that are supposed to help install and update python versions can run into a number 
of path issues when update or installing new versions. This largely stems from attempts to automatically integrate with
the Windows 10 HOMEPATH and APPDATA. For example, a common issue is that the old version keeps opening even
though you installed a newer version. Most simply, let's just look at the Windows menu shortcut for Jupyter Notebook:
It exectures jupyter-notebook-scripy.py and the shortcut specifies where it thinks this file is located.

.. figure:: ./_figure/jupyter_shortcut.png

This points to both %HOMEPATH%	(C:\\Users\\{username}). However, this causes 2 problems. First, computers that are part
of a large business network (e.g. USFS, OSU) have a network homepath as well (N:\\<user> for OSU) where it may be
installed instead or in addition. Second, often programs are installed for all users, and are usually installed in
%PROGRAMDATA%	C:\\ProgramData (same as %ALLUSERSPROFILE%	C:\\ProgramData). To make matters worse, many programs allow
you to
configure a program how you like with
things like default directories and settings. This is all stored somewhere like  %APPDATA%
C:\\Users\\{username}\\AppData\\Roaming.

So to review, when you try to open a program from a windows shortcut (e.g. the windows menu) it will try to grab the
program from the directory the shortcut points to. But once it opens the program, it may grab default settings from an
older version. The 5 places they could be hidden are:

* %ALLUSERSPROFILE%	C:\\ProgramData
* %HOMEPATH%	    C:\\Users\\{username}
* %HOMEPATH%	    Network\\{username}
* %APPDATA%	        C:\\Users\\{username}\\AppData\\Roaming
* %LOCALAPPDATA%	C:\\Users\\{username}\\AppData\\Local

Another source of errors are programs that bundle to be helpful. For example, Anaconda will install Python, Spyder,
and Jupyter automatically. Each IDE has it's own path and .config file. These can all get confused and intermingled.
You may need to untangle each independently. Look at this example of asking Jupyter to reveal its paths.

.. figure:: ./_figure/jupyter_paths.png

It is drawing off of the USERPATH, but it is also drawing from the app settings within this user profile, as well as 2
different program paths.

Extra Jupyter Note
===================

As much as like Jupyter Notebooks and the new Jupyter Labs, I have had trouble getting it to open in the directory I
want. Though I do have a .config file that is sending it to the correct default, it is inflexible in letting me move
to another directory. You can alway start a notebook/lab and use ``%pwd`` and ``%cd`` magics, but that is a silly way for
me to navigate to the notebook I want to open. I have found that this is the key to opening it where I want:
``jupyter notebook --notebook-dir= path/to/my/notebook.ipynb``

If you haven't set up the default path you want yet:

#. ``jupyter notebook --generate-config``
#. Open the jupyter_notebook_config.py and do a ctrl-f search for: *c.NotebookApp.notebook_dir*
#. Uncomment (delete #) and  type `` = my/path`` at the end

