# File: docs/requirements.txt


##################### Why Requirements.txt ##########################################
# ReadTheDocs (RTD) runs sphinx using the python environment on its server.
# As the server gets updated, some packages (libraries for you R folk) won't be backward compatible
# or will increase dependencies/versions of dependent packages that won't be backward compatible.
# This means that RTD may suddenly not be able to build docs for your code!
# If your lucky, this will only apply to old legacy versions. But if you're slow to update, it will break your
# operational code!
# Defining the exact version will make sure things don't break. Because the requirements are stored in git, RTD will
# always know which packages you need to build docs for that version of your code.

# version required to fix bugs
# https://github.com/sphinx-doc/sphinx/issues/9807
# https://blog.readthedocs.com/build-errors-docutils-0-18/
docutils<0.18

# trying to idendity the minimum requirements to pass build
######################### venv freeze ###################################
# virtual environment sphinx doc requirements
# >conda create --name sphinxdoc  python=2.7
# >conda activate sphinxdoc
# >pip install sphinx
# >pip install pandas
# >pip install nbconvert
# >make html
# >pip freeze > requirements.txt

alabaster==0.7.12
Babel==2.9.1
certifi==2020.6.20
chardet==4.0.0
colorama==0.4.4               #not specified by RTD
idna==2.10
imagesize==1.3.0
Jinja2==2.11.3
MarkupSafe==1.1.1
numpy==1.16.6                 #not specified by RTD
packaging==20.9               #version not specified by RTD
pandas==0.24.2                #not specified by RTD
Pygments==2.5.2
pyparsing==2.4.7
python-dateutil==2.8.2        #not specified by RTD
pytz==2021.3
requests==2.26.0
six==1.16.0
snowballstemmer==2.2.0
Sphinx==1.8.6
sphinxcontrib-websupport==1.1.2
typing==3.10.0.0              #not specified by RTD
urllib3==1.26.7
wincertstore==0.2             #not specified by RTD

# ADDED FOR nbconvert/nbsphinx Jupyter Notebook publication
# attrs==21.2.0
# bleach==3.3.1
# configparser==4.0.2
# contextlib2==0.6.0.post1
# decorator==4.4.2
# defusedxml==0.7.1
# entrypoints==0.3
# enum34==1.1.10
# functools32==3.2.3.post2
# importlib-metadata==2.1.2
# ipython-genutils==0.2.0
# jsonschema==3.2.0
# jupyter-core==4.6.3
# mistune==0.8.4
nbconvert==5.6.1
nbformat==4.4.0
nbsphinx==0.5.1
# pandocfilters==1.5.0
# pathlib2==2.3.6
# pyrsistent==0.16.1
# pywin32==228
# scandir==1.10.0
# testpath==0.4.4
# traitlets==4.3.3
# webencodings==0.5.1
# zipp==1.2.0

######################### RTD log- call ###################################
# RTD log- requirements parsed from command executed for last successful build
# [rtd-command-info] start-time: 2021-06-22T01:28:28.776338Z, end-time: 2021-06-22T01:28:38.992885Z, duration: 10,
# exit-code: 0
# /home/docs/checkouts/readthedocs.org/user_builds/im-hobo/envs/latest/bin/python -m pip install --upgrade
# --no-cache-dir -I mock==1.0.1 pillow==5.4.1 alabaster>=0.7,<0.8,!=0.7.5 commonmark==0.8.1 recommonmark==0.5.0 sphinx
# sphinx-rtd-theme readthedocs-sphinx-ext<2.2

# mock==1.0.1
# pillow==5.4.1
# alabaster>=0.7,<0.8,!=0.7.5
# commonmark==0.8.1
# recommonmark==0.5.0
# sphinx
# sphinx-rtd-theme
# readthedocs-sphinx-ext<2.2

######################### RTD log- echo ###################################
# # RTD log- requirements parsed from screen output from above command.
# mock==1.0.1
# pillow==5.4.1                     #not in venv
# alabaster!=0.7.5,<0.8,>=0.7
# commonmark==0.8.1                 #not in venv
# recommonmark==0.5.0               #not in venv
# sphinx
# sphinx-rtd-theme
# readthedocs-sphinx-ext<2.2        #not in venv
# future
# docutils>=0.11
# imagesize
# python_version < "3.5"
# Pygments>=2.0
# Jinja2>=2.3
# packaging
# snowballstemmer>=1.1
# setuptools
# babel!=2.0,>=1.3
# six>=1.5
# requests>=2.0.0
# sphinxcontrib-websupport
# MarkupSafe>=0.23
# pyparsing>=2.0.2
# pytz>=2015.7
# chardet<5,>=3.0.2
# urllib3<1.27,>=1.21.1
# idna<3,>=2.5
# certifi>=2017.4.17