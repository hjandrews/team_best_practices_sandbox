.. Example oriniginally copied from Analysis_precip_TankGauge documentation 
   master file, created by sphinx-quickstart on Mon Mar  9 16:48:47 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#################################
Best Practices Example
#################################
Examples of docs taken from some of our better code and from some from other's code that we've learned the most from.

Last is an example of
`Sphinx auto-doc <https://eikonomega.medium.com/getting-started-with-sphinx-autodoc-part-1-2cebbbca5365>`_ functions
used in the Python modules. There are some great
`starter guides <https://eikonomega.medium.com/getting-started-with-sphinx-autodoc-part-1-2cebbbca5365>`_  out there.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ExecutiveSummary
   RepoSettings
   SecurityHistoryClean
   HandlingAppPasswords
   PythonPathIssues
   modules
   
.. contents::
   :depth: 3

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
